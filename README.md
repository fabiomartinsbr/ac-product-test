# AC-PRODUCT-TEST #

A sample spring-boot restfull application for coding test. The Stack of thecnologies are SrpingMVC, Srping Data JPA, JAX-RS and custom validation exploring java lambda with functional interfaces.

### Prerequisities ###

* Java 8
* Maven (3.2+)

### Running the app

```
mvn spring-boot:run
```

a) Get all products excluding relationships (child products, images)
curl --header "Accept:application/json" http://localhost:8080/products

b) Get all products including specified relationships (child product and/or images)
curl --header "Accept:application/json" http://localhost:8080/products/all

c) Same as 1 using specific product identity
curl --header "Accept:application/json" http://localhost:8080/products/1

d) Same as 2 using specific product identity
curl --header "Accept:application/json" http://localhost:8080/products/1/all

e) Get set of child products for specific product
curl --header "Accept:application/json" http://localhost:8080/products/1/children

f) Get set of images for specific product
curl --header "Accept:application/json" http://localhost:8080/products/1/images

### Running the webapp

Open your browser to the following url: 

```
http://localhost:8080/
```

## Running the tests

```
mvn test
```

## Author

* **Fabio Vianna Martins**

### Who do I talk to? ###

* Repo owner Fábio Martins
* fabiomartinsbr@gmail.com