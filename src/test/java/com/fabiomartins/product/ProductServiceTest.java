package com.fabiomartins.product;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fabiomartins.product.model.Product;
import com.fabiomartins.product.repository.ProductRepository;
import com.fabiomartins.product.service.impl.ProductServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class ProductServiceTest {

	@Mock
	private ProductRepository productRepository;
	
	private ProductServiceImpl productService;
	
	@Before
	public void setup(){
		productRepository = mock(ProductRepository.class);
		productService = new ProductServiceImpl(productRepository);
	}
	
	@Test
	public void testGetAllProduct(){
		List<Product> productList = new ArrayList<Product>();
		productList.add(new Product(1l, "Product 1", "Product Sample 1", null));
		productList.add(new Product(2l, "Product 2", "Product Sample 2", null));
		productList.add(new Product(3l, "Product 3", "Product Sample 3", null));
		productList.add(new Product(4l, "Product 4", "Product Sample 4", null));;
		when(productRepository.findAll()).thenReturn(productList);
		
		List<Product> result = productService.getAll();
		assertEquals(4, result.size());
	}
	
	@Test
	public void testGetAllProductWithChildren(){
		List<Product> productList = new ArrayList<Product>();
		productList.add(new Product(1l, "Product 1", "Product Sample 1", null));
		productList.add(new Product(2l, "Product 2", "Product Sample 2", null));
		when(productRepository.getAllWithChildre()).thenReturn(productList);
		
		List<Product> result = productService.getAllWithChildren();
		assertEquals(2, result.size());
	}
	
	@Test
	public void testGetChildsById(){
		List<Product> productList = new ArrayList<Product>();
		productList.add(new Product(1l, "Product 1", "Product Sample 1", null));
		productList.add(new Product(2l, "Product 2", "Product Sample 2", null));
		when(productRepository.getChildrenById(1L)).thenReturn(productList);
		
		List<Product> result = productService.getChildrenById(1L);
		assertEquals(2, result.size());
	}
	
	@Test
	public void testGetProductById(){
		Product product = new Product(1l, "Product 1", "Product sample 1", null);
		when(productRepository.findOne(1L)).thenReturn(product);
		Product result = productService.getById(1l);
		
		assertEquals(true, new Long(1).equals(result.getId()));
		assertEquals("Product 1", result.getName());
		assertEquals("Product sample 1", result.getDescription());
	}
	
	@Test
	public void addProduct(){
		Product product = new Product(7l, "Product 7", "Product sample 7", null);
		when(productRepository.save(product)).thenReturn(product);
		Product result = productService.add(product);
		assertEquals(true, new Long(7).equals(result.getId()));
		assertEquals("Product 7", result.getName());
		assertEquals("Product sample 7", result.getDescription());
	}
	
	@Test
	public void updateProduct(){
		Product product = new Product(8l, "Product 8", "Product Sample 8", null);
		when(productRepository.save(product)).thenReturn(product);
		Product result = productService.update(product);
		assertEquals(true, new Long(8).equals(result.getId()));
		assertEquals("Product 8", result.getName());
		assertEquals("Product Sample 8", result.getDescription());
	}
	
	@Test
	public void removeProduct(){
		Product Product = new Product(9l, "Product 9", "Product Sample 9", null);
		productService.remove(Product);
        verify(productRepository, times(1)).delete(Product);
	}
}
