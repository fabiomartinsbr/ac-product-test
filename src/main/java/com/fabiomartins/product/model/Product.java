package com.fabiomartins.product.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fabiomartins.product.util.ProductViews;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Product {

	@Id
	@GeneratedValue
	@JsonView(ProductViews.ProductBasics.class)
	private Long id;
	
	@Column
	@JsonView(ProductViews.ProductBasics.class)
	private String name;
	
	@Column
	@JsonView(ProductViews.ProductBasics.class)
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumn(name="parent_id")
	@JsonBackReference
	private Product parent;
	
	@OneToMany(mappedBy="parent", orphanRemoval = true, cascade=CascadeType.ALL)
	@OrderBy("id")
	private Set<Product> children = new HashSet<Product>();
	
	@OrderBy("id")
	@OneToMany(mappedBy="product", orphanRemoval = true, cascade = CascadeType.ALL)
	private Set<Image> images = new HashSet<Image>();
	
	public Product() {
		
	}
	
	public Product(Long id, String name, String description, Product parent) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.parent = parent;
	}
	
	public Product(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Product(String name, String description, Product parent) {
		super();
		this.name = name;
		this.description = description;
		this.parent = parent;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Product getParent() {
		return parent;
	}
	
	public void setParent(Product parent) {
		this.parent = parent;
	}

	public Set<Image> getImages() {
		return images;
	}
	
	public Set<Product> getChildren() {
		return children;
	}

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getId());
        builder.append(getName());
        builder.append(getDescription());
        builder.append(getParent());
        return builder.hashCode();
    };

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Product)) {
            return false;
        }
		Product other = (Product) obj;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(id, other.id);
		builder.append(name, other.name);
		builder.append(description, other.description);
		builder.append(parent, other.parent);
		
		return builder.isEquals();
	}
	
	
}
