package com.fabiomartins.product.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabiomartins.product.model.Product;
import com.fabiomartins.product.repository.ProductRepository;
import com.fabiomartins.product.service.ProductService;


@Service("productService")
public class ProductServiceImpl implements ProductService {

	private ProductRepository productRepository;
	
	@Autowired
	public ProductServiceImpl(ProductRepository repository){
		this.productRepository = repository;
	}
	
	@Override
	public List<Product> getAll() {
		return productRepository.findAll();
	}

	@Override
	public List<Product> getAllWithChildren() {
		return productRepository.getAllWithChildre();
	}

	@Override
	public Product getById(Long id) {
		return productRepository.findOne(id);
	}

	@Override
	public List<Product> getChildrenById(Long id) {
		return productRepository.getChildrenById(id);
	}

	@Override
	@Transactional
	public Product add(Product product) {
		return productRepository.save(product);
	}
	
	@Transactional
	@Override
	public Product update(Product product) {
		return productRepository.save(product);
		
	}
	

	@Override
	@Transactional
	public void remove(Product product) {
		productRepository.delete(product);
	}

	
}
