package com.fabiomartins.product.service;

import java.util.List;

import com.fabiomartins.product.model.Product;

public interface ProductService {

	List<Product> getAll();
	List<Product> getAllWithChildren();
	Product getById(Long id);
	List<Product> getChildrenById(Long id);
	Product add(Product product);
	Product update(Product product);
	void remove(Product product);
}
