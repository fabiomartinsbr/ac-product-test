package com.fabiomartins.product.controller;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fabiomartins.product.exceptions.ProductException;
import com.fabiomartins.product.model.Image;
import com.fabiomartins.product.model.Product;
import com.fabiomartins.product.model.Response;
import com.fabiomartins.product.service.ProductService;
import com.fabiomartins.product.util.ProductValidation;
import com.fabiomartins.product.util.ProductViews;
import com.fabiomartins.product.util.ValidationResult;
import com.fasterxml.jackson.annotation.JsonView;



@RestController
public class ProductController {

	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	@JsonView(ProductViews.ProductBasics.class)
	@RequestMapping(path = "/products", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProductsWithBasicInformation() {
		logger.info("returning all products with basic informations.");
		
		List<Product> products = productService.getAll();
		
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@RequestMapping(path = "/products/all", method = RequestMethod.GET)
	public  ResponseEntity<List<Product>>  getProductsWithAllInformation() {
		logger.info("returning all products with complete informations.");

		return new ResponseEntity<List<Product>>(productService.getAllWithChildren(), HttpStatus.OK);
	}

	@JsonView(ProductViews.ProductBasics.class)
	@RequestMapping(path = "/products/{id}", method = RequestMethod.GET)
	public ResponseEntity<Product> getBasicInfoById(@PathVariable("id") long id) throws ProductException {
		logger.info("returning a basic product by id.");
		
		Product product = productService.getById(id);
		
		ValidationResult validationResult = ProductValidation.RestValidation.isProductExists().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}

		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@RequestMapping(path = "/products/{id}/all", method = RequestMethod.GET)
	public ResponseEntity<Product> getById(@PathVariable("id") long id) throws Exception {
		logger.info("returning all information about product by id...");

		Product product = productService.getById(id);

		ValidationResult validationResult = ProductValidation.RestValidation.isProductExists().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}

		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/products/{id}/children", method = RequestMethod.GET)
	public ResponseEntity<Set<Product>> getChildrenByIdProduct(@PathVariable("id") long id) throws ProductException {
		logger.info("returning all children by productId.");

		Product product = productService.getById(id);

		ValidationResult validationResult = ProductValidation.RestValidation.isProductExists().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}
		Set<Product> products = product.getChildren(); //productService.getChildsById(id);
		return new ResponseEntity<Set<Product>>(products, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/products/{id}/images", method = RequestMethod.GET)
	public ResponseEntity<Set<Image>> getImagesByIdProduct(@PathVariable("id") long id) throws ProductException {
		logger.info("returning a product by id...");

		Product product = productService.getById(id);

		ValidationResult validationResult = ProductValidation.RestValidation.isProductExists().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}

		return new ResponseEntity<Set<Image>>(product.getImages(), HttpStatus.OK);
	}

	@RequestMapping(path = "/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response> remove(@PathVariable("id") long id) throws ProductException {
		logger.info("deleting a product by id...");

		Product product = productService.getById(id);

		ValidationResult validationResult = ProductValidation.RestValidation.isProductExists().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}

		productService.remove(product);

		return new ResponseEntity<Response>(
				new Response(HttpStatus.OK.value(), 
				"Product has just deleted."),
				HttpStatus.OK);
	}

	@RequestMapping(path = "/products", method = RequestMethod.POST)
	public ResponseEntity<Product> add(@RequestBody Product product) throws ProductException {
		logger.info("add product  " + product);

		ValidationResult validationResult = ProductValidation.RestValidation.isCreatePayloadValid().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}
		
		applyFetchChildrenWithParend(product);

		return new ResponseEntity<Product>(productService.add(product), HttpStatus.OK);
	}
	
	
	private void applyFetchChildrenWithParend(Product product){
		product.getChildren().stream().forEach(child -> child.setParent(product));
		product.getImages().stream().forEach(child -> child.setProduct(product));
		/*if(product.getParent() != null && product.getParent().getId() > 0){
			applyFetchChildrenWithParend(product.getParent());
		}*/
	}

	@RequestMapping(path = "/products", method = RequestMethod.PATCH)
	public ResponseEntity<Product> update(@RequestBody Product p) throws ProductException {
		logger.info("product to update " + p);

		ValidationResult validationResult = ProductValidation.RestValidation.isUpdatePayloadValid().apply(p);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}

		Product product = productService.getById(p.getId());
		validationResult = ProductValidation.RestValidation.isProductExists().apply(product);
		if (!validationResult.isValid()) {
			throw new ProductException(validationResult.getViolations().get());
		}
		
		applyFetchChildrenWithParend(p);
		

		return new ResponseEntity<Product>(productService.update(p), HttpStatus.OK);
	}

}
