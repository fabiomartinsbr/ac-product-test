package com.fabiomartins.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fabiomartins.product.model.Product;

@Repository("productRepository")
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("SELECT distinct p FROM Product p where p.parent is null order by p.id")
	List<Product> findAll();
	
	@Query("SELECT distinct p FROM Product p LEFT JOIN FETCH p.children LEFT JOIN FETCH p.images where (p.parent is null) order by p.id")
	List<Product> getAllWithChildre();
	
	@Query("SELECT distinct p FROM Product p LEFT JOIN FETCH p.children LEFT JOIN FETCH p.images where p.parent.id = ?1 order by p.id")
	List<Product> getChildrenById(Long id);
	
	@Query("SELECT distinct p FROM Product p LEFT JOIN FETCH p.children LEFT JOIN FETCH p.images where p.id = ?1 order by p.id")
	Product findOne(Long id);
	
}
