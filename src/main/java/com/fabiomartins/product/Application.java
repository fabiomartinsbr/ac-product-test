package com.fabiomartins.product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fabiomartins.product.model.Image;
import com.fabiomartins.product.model.Product;
import com.fabiomartins.product.repository.ProductRepository;

@SpringBootApplication
public class Application   {
	public static List<Product> producs = new ArrayList<Product>();
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
            	registry.addMapping("/*").allowedMethods("GET", "POST", "PATCH", "DELETE", "HEAD").allowedOrigins("*");
            	registry.addMapping("/*/*").allowedMethods("GET", "POST", "PATCH", "DELETE", "HEAD").allowedOrigins("*");
            	registry.addMapping("/*/*/*").allowedMethods("GET", "POST", "PATCH", "DELETE", "HEAD").allowedOrigins("*");
            }
        };
    }
	
	@Bean
	@Transactional
	public CommandLineRunner initialize(ProductRepository productRepository) {
		return (args) -> {
			Arrays.asList(1,2,3).stream().forEach(ind -> {
				
				final Product product = new Product("Product " + ind, "Product number " + ind, null);
				product.getImages().add(new Image("IMAGE1", product));
				product.getImages().add(new Image("IMAGE2", product));
				final Product parent = addProduct(productRepository, product);
				
				Arrays.asList(1,2).stream().forEach(indChild -> {
					addProduct(productRepository, new Product("Child Product " + indChild, "Child Product number " + indChild + " of Parent " + parent.getName(), parent));
				});
			});
			
			logger.info("Data has just generated!!");
		};
	}
	
	@Transactional
	private Product addProduct(ProductRepository productRepository, Product p){
		return productRepository.saveAndFlush(p);
	}
	
}
