(function () {
    angular.module('myApp').factory('msgs', [
        'toastr',
        MsgsFactory
    ]);


    function MsgsFactory(toastr) {

      function addMsgs(msgs, title, method) {
          if(msgs instanceof Array) {
              msgs.forEach( msg => toastr[method](msg, title) );
          } else {
              toastr[method](msgs, title);
          }
      };

      function addSuccess(msgs) {
          addMsgs(msgs, 'Success', 'success');
      };

      function addError(msgs) {
          addMsgs(msgs, 'Error', 'error');
      };

      return { addSuccess, addError };

    };

})()