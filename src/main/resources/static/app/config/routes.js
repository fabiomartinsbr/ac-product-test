(function () {
    
	angular.module('myApp').config([
	    '$stateProvider',
	    '$urlRouterProvider', 
	    function ($stateProvider, $urlRouterProvider) {
	        
	        $stateProvider.state('product', {
	            url:"/product",
	            templateUrl:"app/product/tabs.html"
	        });

	    }
	]);

})()

