(function () {
    
    angular.module('myApp').controller('ProductCtrl', [
        '$http',
        'msgs',
        'tabs',
        '$location',
        ProductController
    ]);

    function ProductController($http, msgs, tabs, $location) {
        const vm = this;
        const _url =  "http://localhost:8080/products/";

        vm.refresh = function() {

            $http.get(`${_url}`)
                .then(function (response) {
                	vm.product = {children:[{}] , images:[{}] };
                    vm.products = response.data;
                    tabs.show( vm,  { tabList:true, tabCreate:true} );
                });

        };
        
        vm.showTabUpdate = function (product) {
            $http.get(`${_url}/${product.id}/all`)
            .then(function (response) {
                vm.product = response.data;
                tabs.show(vm, {tabUpdate:true});
            });        	
        	
            //vm.product = product;
            //tabs.show(vm, {tabUpdate:true});
        };

        vm.showTabDelete = function (product) {
            $http.get(`${_url}/${product.id}/all`)
            .then(function (response) {
                vm.product = response.data;
                tabs.show(vm, {tabDelete:true});
            }); 
            
            //vm.product = product;
            //tabs.show(vm, {tabDelete:true});
        };
        
        vm.addImage = function (index) {
            vm.product.images.splice(index +1, 0, {});
        }
        
        vm.deleteImage = function (index) {
            if(vm.product.images.length > 0) {
                vm.product.images.splice(index, 1);  
                if (vm.product.images.length == 0) {
                	vm.product.images = [{}];
                }
            }
        }
        
        vm.addChild = function (index) {
            vm.product.children.splice(index +1, 0, {});
        }

        vm.deleteChild = function (index) {
            if(vm.product.children.length > 0) {
                vm.product.children.splice(index, 1);
                if (vm.product.children.length == 0) {
                	vm.product.children = [{}];
                }
            }
        }
        
        vm.create = function () {

            $http.post(_url, vm.product)
                .then(function (response) {
                    vm.refresh();
                    msgs.addSuccess('Operation has been successful!');
                })
                .catch(function (response) {
                    msgs.addError(response.data.message)
                });

        }
        
        vm.delete = function () {
            const deleteUrl = `${_url}/${vm.product.id}`;

            $http.delete(deleteUrl, vm.product)
                .then(function (response) {
                    vm.refresh();
                    msgs.addSuccess('Operation has been successful!');
                })
                .catch(function (response) {
                    msgs.addError(response.data.message);
                });

        };        
        
        vm.update = function () {
            const updateUrl = `${_url}`;

            $http.patch(updateUrl, vm.product)
                .then(function (response) {
                    vm.refresh();
                    msgs.addSuccess('Operation has been successful!');
                })
                .catch(function (response)
                 {
                    msgs.addError(response.data.message);
                });

        };
        
        vm.refresh();

    }

})()